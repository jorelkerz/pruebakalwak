import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pruebas: '',
    tema: '',
    crearPrueba: false,
    mensaje: { color: "", texto: "" },
    dismissSecs: 3,
    dismissCountDown: 0
  },
  mutations: {
    countDownChanged(state, dismissCountDown) {
      state.dismissCountDown = dismissCountDown;
    },
    showAlert(state) {
      // console.log('dismissCountDown:', state.dismissCountDown)
      state.dismissCountDown = state.dismissSecs;
    },
  },
  actions: {
  },
  modules: {
  }
})
