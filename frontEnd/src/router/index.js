import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: {
      name: 'Practica',
      component: () => import('../views/Practica.vue')
    }
    // name: 'Home',
    // component: Home
  },
  {
    path: '/curso',
    name: 'Curso',
    component: () => import(/* webpackChunkName: "prueba" */ '../views/Curso.vue')
  },
  {
    path: '/practica',
    name: 'Practica',
    component: () => import('../views/Practica.vue')
  },
  {
    path: '/administra',
    name: 'Administra',
    component: () => import('../views/Administra.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
