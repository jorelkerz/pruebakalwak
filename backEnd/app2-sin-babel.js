const express = require('express');
const app = express();
const path = require('path');

//Nos sirve para pintar las peticiones HTTP request que se solicitan a nuestro aplicación.
const morgan = require('morgan');
app.use(morgan('tiny'));

//Para realizar solicitudes de un servidor externo e impedir el bloqueo por CORS.
const cors = require('cors');
app.use(cors());

app.use(express.json());
//para trabajar con aplicaciones
//application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));


// Rutas
app.get('/', function (req, res) {
    res.send('Hello World!');
});

//************************ */
// Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback');
app.use(history());
//se acede a la carpeta public
app.use(express.static(path.join(__dirname, 'public')));
//************************ */


app.set('puerto', process.env.PORT || 3000);
app.listen(app.get('puerto'), function () {
    console.log('Example app listening on port' + app.get('puerto'));
});
