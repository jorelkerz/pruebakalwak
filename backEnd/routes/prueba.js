import express from 'express';
const router = express.Router();

// importar el modelo prueba
import Prueba from '../models/prueba';

// Agregar una prueba
router.post('/nueva-prueba', async (req, res) => {
    const body = req.body;
    try {
        const pruebaDB = await Prueba.create(body);
        res.status(200).json(pruebaDB);
    } catch (error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Get con parámetros
router.get('/prueba/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const pruebaDB = await Prueba.findOne({ _id });
        res.json(pruebaDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Get con todos los documentos
router.get('/prueba', async (req, res) => {
    try {
        const pruebaDB = await Prueba.find();
        res.json(pruebaDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Delete eliminar una prueba
router.delete('/prueba/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const pruebaDB = await Prueba.findByIdAndDelete({ _id });
        if (!pruebaDB) {
            return res.status(400).json({
                mensaje: 'No se encontró el id indicado',
                error
            })
        }
        res.json(pruebaDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Put actualizar una prueba
router.put('/prueba/:id', async (req, res) => {
    const _id = req.params.id;
    const body = req.body;
    console.log('body: ', req.body);
    try {
        const pruebaDB = await Prueba.findByIdAndUpdate(
            _id,
            body,
            { new: true });
        //{ new: true }); retorna la prueba actualizada
        res.json(pruebaDB);
        console.log(res.json(pruebaDB));

    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Exportamos la configuración de express app
module.exports = router;