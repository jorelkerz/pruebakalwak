import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const pruebaSchema = new Schema({
    tema: { type: String, required: [true, 'Tema obligatorio'] },
    preguntas: [{ pregunta: String, respuestas: [{ respuesta: String, condicion: Boolean }] }],
    usuarioId: String,
    date: { type: Date, default: Date.now },
    activo: { type: Boolean, default: true }
});

// Convertir a modelo
const Prueba = mongoose.model('pruebas', pruebaSchema);

export default Prueba;